from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def project(request):
    return render(request, 'main/project.html')
