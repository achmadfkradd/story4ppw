from django.urls import path
from django.conf.urls import include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('project/', views.project, name='project'),
    path('matkul/',include('story5yaallah.urls'))
]
