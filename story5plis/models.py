from django.db import models

# Create your models here.

class Matkul(models.Model):
    Nama = models.TextField(max_length=50)
    Dosen = models.TextField(max_length=50)
    JumlahSKS = models.IntegerField()
    Deskripsi = models.TextField(max_length=60)
    Periode = models.TextField(max_length=30)
    Kelas = models.TextField(max_length=50)
