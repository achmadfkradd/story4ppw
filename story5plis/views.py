from django.shortcuts import render,redirect
from .models import Matkul
from .forms import MatkulForm

# Create your views here.
def create(request):
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            matkul = Matkul()
            matkul.Nama = form.cleaned_data['Nama']
            matkul.Dosen = form.cleaned_data['Dosen']
            matkul.JumlahSKS = form.cleaned_data['JumlahSKS']
            matkul.Deskripsi = form.cleaned_data['Deskripsi']
            matkul.Periode = form.cleaned_data['Periode']
            matkul.Kelas = form.cleaned_data['Kelas']
            matkul.save()
        return redirect('/matkul')
    else:
        data = Matkul.objects.all()
        form = MatkulForm()
        response = {"matkul":data, 'form' : form}
        return render(request,'matakuliah.html',response)

def delete(request,pk):
        Matkul.objects.get(pk = pk).delete()
        return redirect('/matkul')

def details(request,pk):
        data = Matkul.objects.get(pk = pk)
        response = {"matkul":data}
        return render(request, 'details.html', response)
