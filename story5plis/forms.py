from django import forms
        
class MatkulForm(forms.Form):

    Nama = forms.CharField(label="Nama Mata Kuliah", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    Dosen = forms.CharField(label="Nama Dosen", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    JumlahSKS = forms.IntegerField(label="Jumlah SKS",widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'number',
        'required': True,
    }))
    
    Deskripsi = forms.CharField(label="Deskripsi Mata Kuliah", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    Periode = forms.CharField(label="Periode Tahun/Semester", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Semester',
        'type' : 'text',
        'required': True,
    }))

    Kelas = forms.CharField(label="Ruang Kelas", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Ruang Kelas',
        'type' : 'text',
        'required': True,
    }))
