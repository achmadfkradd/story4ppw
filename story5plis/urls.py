from django.urls import path, re_path
from .views import create, delete, details

appname = 'Story5plis'

urlpatterns = [
   path('', create, name = 'create'),
   path('<int:pk>/delete/', delete),
   path('<int:pk>/', details, name='xx'),
]
