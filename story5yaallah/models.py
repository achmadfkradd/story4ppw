from django.db import models

# Create your models here.

PERIODE = (
        ('gasal1819', 'Semester Gasal 2018/2019'),
        ('genap1819', 'Semester Genap 2018/2019'),
        ('gasal1920', 'Semester Gasal 2019/2020'),
        ('genap1920', 'Semester Genap 2019/2020'),
        ('gasal2021', 'Semester Gasal 2020/2021'),
    )

class Matkul(models.Model):
    Nama = models.TextField(max_length=50)
    Dosen = models.TextField(max_length=50)
    JumlahSKS = models.TextField(max_length=5)
    Deskripsi = models.TextField(max_length=60)
    Periode = models.CharField(max_length=10, choices=PERIODE)
    Kelas = models.TextField(max_length=50)
