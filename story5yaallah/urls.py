from django.urls import path, re_path
from .views import create, delete, details

appname = 'Story5yaallah'

urlpatterns = [
   path('', create, name = 'create'),
   path('<int:pk>/delete/', delete),
   path('<int:pk>/', details, name='xx'),
]
