from django.db import models

# Create your models here.
class Matkul(models.Model):
    Nama = models.TextField(max_length=50)
    Dosen = models.TextField(max_length=50)
    JumlahSKS = models.TextField(max_length=5)
    Deskripsi = models.TextField(max_length=150)
    Periode = models.TextField(max_length=30)
    Kelas = models.TextField(max_length=50)
